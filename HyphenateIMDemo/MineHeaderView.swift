//
//  MineHeaderView.swift
//  HyphenateIMDemo
//
//  Created by Shang Hai Rui Xi on 2019/1/4.
//  Copyright © 2019 Shang Hai Rui Xi. All rights reserved.
//

import UIKit

class MineHeaderView: UIView {

    override func layoutSubviews() {
        super.layoutSubviews()
        self.frame.size.height = 150
    }

}
