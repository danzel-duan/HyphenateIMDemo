//
//  FunctionsView.swift
//  HyphenateIMDemo
//
//  Created by Shang Hai Rui Xi on 2019/2/19.
//  Copyright © 2019 Shang Hai Rui Xi. All rights reserved.
//

import UIKit

enum FunctionType {
    case photo
}

protocol FunctionViewDelegate: NSObjectProtocol {
    func functionView(view: FunctionsView, didSelectFunctionType type: FunctionType)
}

class FunctionsView: UIView {
    public var delegate: FunctionViewDelegate?
    /// 顶部分割线
    private var line : UIView = UIView()
    private var types: [FunctionType] = [.photo]
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubviews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func addSubviews() {
        backgroundColor = MYColorForRGB(244, 244, 244)
        line.frame = .init(x: 0, y: 0, width: my.width, height: 0.5)
        line.backgroundColor = MYColorForRGB(211, 211, 211)
        addSubview(line)
        addSubview(collectionView)
    }
    
    private lazy var collectionView: UICollectionView = {
        let flowLayout = UICollectionViewFlowLayout.init()
        flowLayout.minimumLineSpacing = 10
        flowLayout.minimumInteritemSpacing = 10
        flowLayout.scrollDirection = .horizontal
        let collectionView = UICollectionView.init(frame: CGRect.init(x: 0, y: MYStickerTopSpace, width: my.width, height: MYStickerScrollerHeight), collectionViewLayout: flowLayout)
        collectionView.backgroundColor = .clear
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.bounces = false
        collectionView.isPagingEnabled = true
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.contentInset = UIEdgeInsets.init(top: 0, left: 10, bottom: 0, right: 10)
        collectionView.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "cellID")
        return collectionView
    }()
}

extension FunctionsView: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return types.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellID", for: indexPath)
        cell.backgroundColor = UIColor.green
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        delegate?.functionView(view: self, didSelectFunctionType: types[indexPath.row])
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize.init(width: MYSCREEN_WIDTH / 4, height: MYSCREEN_WIDTH / 4)
    }
}
