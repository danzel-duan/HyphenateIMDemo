//
/*******************************************************************************
 
 File name:     BaseInputView.swift
 Author:        danzel_duan
 Gitee.com:     https://gitee.com/danzel-duan/YBIMKit
 E-mail:        danzel_duan@163.com
 
 Description: Copyright © danzel_duan. All rights reserved.
 
 Created by danzel on 2018/12/17.
 
 ********************************************************************************/

import UIKit

public func layoutConstraintMake(_ item: AnyObject, _ attr1: NSLayoutConstraint.Attribute, _ related: NSLayoutConstraint.Relation, _ toItem: AnyObject? = nil, _ attr2: NSLayoutConstraint.Attribute = .notAnAttribute, _ constant: CGFloat = 0, priority: UILayoutPriority = UILayoutPriority.init(1000), multiplier: CGFloat = 1) -> NSLayoutConstraint {
    
    if let view = item as? UIView {
        view.translatesAutoresizingMaskIntoConstraints = false
    }
    
    let constraint = NSLayoutConstraint(item:item, attribute:attr1, relatedBy:related, toItem:toItem, attribute:attr2, multiplier:multiplier, constant:constant)
    constraint.priority = priority
    
    return constraint
}
