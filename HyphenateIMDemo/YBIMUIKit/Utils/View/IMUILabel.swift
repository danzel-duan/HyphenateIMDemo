//
/*******************************************************************************
 
 File name:     BaseInputView.swift
 Author:        danzel_duan
 Gitee.com:     https://gitee.com/danzel-duan/YBIMKit
 E-mail:        danzel_duan@163.com
 
 Description: Copyright © danzel_duan. All rights reserved.
 
 Created by danzel on 2018/12/17.
 
 ********************************************************************************/

import UIKit

public class IMUILabel: UILabel {

    /// 文字内边距: 默认
    public var textInsets: UIEdgeInsets = UIEdgeInsets.init(top: 3, left: 5, bottom: 3, right: 5)
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override public func drawText(in rect: CGRect) {
        super.drawText(in: rect.inset(by: textInsets))
    }
    
    override public func textRect(forBounds bounds: CGRect, limitedToNumberOfLines numberOfLines: Int) -> CGRect {
        var rect = super.textRect(forBounds: bounds, limitedToNumberOfLines: numberOfLines)
        rect.origin.x -= textInsets.left
        rect.origin.y -= textInsets.top
        rect.size.width += (textInsets.left + textInsets.right)
        rect.size.height += (textInsets.top + textInsets.bottom)
        return rect
    }
}
