//
/*******************************************************************************
 
 File name:     BaseInputView.swift
 Author:        danzel_duan
 Gitee.com:     https://gitee.com/danzel-duan/YBIMKit
 E-mail:        danzel_duan@163.com
 
 Description: Copyright © danzel_duan. All rights reserved.
 
 Created by danzel on 2018/12/17.
 
 ********************************************************************************/

import UIKit

////- 增添用户头像和姓名区域及消息自定义区域(UI设计)
//  ┌───────────────────────────────────────────────────────────┐
//  │    ┌─────────────────────────────────────────────────┐    │
//  │    │          timeLabel:show time                    │    │
//  │    └─────────────────────────────────────────────────┘    │
//  ┣───────────────────────────────────────────────────────────┫
//  ┣───────────────────────────────────────────────────────────┫
//  │ ┌───┐ ######                                              │
//  │ │   │ ┌─────────────────────────────────────┐             │
//  │ └───┘ │                                     │             │
//  │       │                                     │             │
//  │       │                                     │             │
//  │       │                                     │             │
//  │       │                                     │             │
//  │       │                                     │             │
//  │       └─────────────────────────────────────┘             │
//  ┣───────────────────────────────────────────────────────────┫
//  └───────────────────────────────────────────────────────────┘

public class MessageCell: MessageBaseCell {
    
    public var avatarView:         MessageAvatarContentView!
    public var nickNameLabel:      UILabel!
    public var messageContentView: MessageContentView!
    public var isHiddenName: Bool = false {
        didSet {
            if oldValue == isHiddenName { return }
            adjustConstraints(isHiddenName: isHiddenName)
        }
    }
    public var isAdjustItemSiteAtLeft: Bool = true {
        didSet {
            if oldValue == isAdjustItemSiteAtLeft { return }
            adjustConstraints(isAdjustItemSiteAtLeft)
        }
    }
    
    /// 约束
    private var avatarViewLeftConstraint:          NSLayoutConstraint!
    private var avatarViewRightConstraint:         NSLayoutConstraint!
    private var nickNameLabelLeftConstraint:       NSLayoutConstraint!
    private var nickNameLabelRightConstraint:      NSLayoutConstraint!
    private var messageContentViewLeftConstraint:  NSLayoutConstraint!
    private var messageContentViewRightConstraint: NSLayoutConstraint!
    private var messageContentViewTopConstraint:   NSLayoutConstraint!
    private var messageContentViewTop1Constraint:  NSLayoutConstraint!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    /// 调整内容左右位置
    ///
    /// - Parameter isAtLeft: bool
    private func adjustConstraints(_ isAtLeft: Bool) {
        if !isAtLeft {
            NSLayoutConstraint.deactivate([avatarViewLeftConstraint, nickNameLabelLeftConstraint, messageContentViewLeftConstraint])
            addConstraints([avatarViewRightConstraint, nickNameLabelRightConstraint, messageContentViewRightConstraint])
        } else {
            NSLayoutConstraint.deactivate([avatarViewRightConstraint, nickNameLabelRightConstraint, messageContentViewRightConstraint])
            addConstraints([avatarViewLeftConstraint, nickNameLabelLeftConstraint, messageContentViewLeftConstraint])
        }
    }
    
    /// 隐藏用户名
    ///
    /// - Parameter isHiddenName: bool
    private func adjustConstraints(isHiddenName: Bool) {
        if isHiddenName {
            NSLayoutConstraint.deactivate([messageContentViewTopConstraint])
            addConstraint(messageContentViewTop1Constraint)
        } else {
            NSLayoutConstraint.deactivate([messageContentViewTop1Constraint])
            addConstraint(messageContentViewTopConstraint)
        }
        nickNameLabel.isHidden = isHiddenName
    }
}

//MARK: 设置UI
extension MessageCell {
    override public func prepare() {
        super.prepare()
        
        avatarView = MessageAvatarContentView.init()
        avatarView.backgroundColor = .lightGray
        
        nickNameLabel = UILabel.init()
        nickNameLabel.text = "nickNameLabel"
        nickNameLabel.font = UIFont.systemFont(ofSize: 14)
        nickNameLabel.textColor = UIColor.black
        nickNameLabel.textAlignment = .center
        
        messageContentView = MessageContentView.init()
        
        baseContentView.addSubview(avatarView)
        baseContentView.addSubview(nickNameLabel)
        baseContentView.addSubview(messageContentView)
    }
    
    override public func layout() {
        super.layout()
        //布局
        avatarViewLeftConstraint  = layoutConstraintMake(avatarView, .left, .equal, baseContentView, .left, 10)
        avatarViewRightConstraint = layoutConstraintMake(avatarView, .right, .equal, baseContentView, .right, -10)
        addConstraint(avatarViewLeftConstraint)
        addConstraint(layoutConstraintMake(avatarView, .top, .equal, baseContentView, .top, 10))
        addConstraint(layoutConstraintMake(avatarView, .width, .equal, nil, .notAnAttribute, 40))
        addConstraint(layoutConstraintMake(avatarView, .height, .equal, nil, .notAnAttribute, 40))
        
        nickNameLabelLeftConstraint  = layoutConstraintMake(nickNameLabel, .left, .equal, avatarView, .right, 6)
        nickNameLabelRightConstraint = layoutConstraintMake(nickNameLabel, .right, .equal, avatarView, .left, -6)
        addConstraint(nickNameLabelLeftConstraint)
        addConstraint(layoutConstraintMake(nickNameLabel, .top, .equal, avatarView, .top))
        addConstraint(layoutConstraintMake(nickNameLabel, .height, .lessThanOrEqual, nil, .notAnAttribute, 18))
        
        messageContentViewLeftConstraint  = layoutConstraintMake(messageContentView, .left, .equal, avatarView, .right, 6)
        messageContentViewRightConstraint = layoutConstraintMake(messageContentView, .right, .equal, avatarView, .left, -6)
        messageContentViewTopConstraint = layoutConstraintMake(messageContentView, .top, .equal, nickNameLabel, .bottom, 5)
        messageContentViewTop1Constraint = layoutConstraintMake(messageContentView, .top, .equal, avatarView, .top)
        addConstraint(messageContentViewLeftConstraint)
        addConstraint(messageContentViewTopConstraint)
        addConstraint(layoutConstraintMake(messageContentView, .bottom, .equal, baseContentView, .bottom))
        addConstraint(layoutConstraintMake(messageContentView, .width, .lessThanOrEqual, baseContentView, .width, priority: .init(900), multiplier: 0.687))
    }
}
