//
/*******************************************************************************
 
 File name:     BaseInputView.swift
 Author:        danzel_duan
 Gitee.com:     https://gitee.com/danzel-duan/YBIMKit
 E-mail:        danzel_duan@163.com
 
 Description: Copyright © danzel_duan. All rights reserved.
 
 Created by danzel on 2018/12/17.
 
 ********************************************************************************/

import UIKit

/////-- 基础消息cell(UI设计)
//  ┌───────────────────────────────────────────────────────────┐  ---
//  │    ┌─────────────────────────────────────────────────┐    │   |
//  │    │          timeLabel:show time                    │    │ -----> timeView
//  │    └─────────────────────────────────────────────────┘    │   |
//  ┣───────────────────────────────────────────────────────────┫  ---
//  ┣───────────────────────────────────────────────────────────┫
//  │                                                           │
//  │                                                           │
//  │                                                           │
//  │                                                           │
//  │               baseContentView: Custom area                │
//  │                                                           │
//  │                                                           │
//  │                                                           │
//  │                                                           │
//  ┣───────────────────────────────────────────────────────────┫
//  └───────────────────────────────────────────────────────────┘

public class MessageBaseCell: UICollectionViewCell {
    
    public var timeView: UIView!
    public var timeLabel: MessageTimeContentView!
    public var baseContentView: UIView!
    
    /// 是否隐藏时间
    public var isHiddenTime: Bool = false {
        didSet {
            if oldValue == isHiddenTime { return }
            adjustConstraints()
        }
    }
    
    ///- 约束
    private var timeViewHeightConstraint: NSLayoutConstraint = NSLayoutConstraint.init()
    private var baseContentViewTopConstraint: NSLayoutConstraint = NSLayoutConstraint.init()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        prepare()
        layout()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    /// 调整约束
    private func adjustConstraints() {
        if isHiddenTime {
            timeViewHeightConstraint.constant = 0
            baseContentViewTopConstraint.constant = 0
        } else {
            timeViewHeightConstraint.constant = 35
            baseContentViewTopConstraint.constant = 5
        }
        timeLabel.isHidden = isHiddenTime
    }
}

//MARK: 设置UI
extension MessageBaseCell {
    
    @objc
    public func prepare() {
        /// 初始化
        timeView = UIView.init()
    
        timeLabel = MessageTimeContentView.init()
        
        baseContentView = UIView.init()
        /// 添加子控件
        contentView.addSubview(timeView)
        timeView.addSubview(timeLabel)
        contentView.addSubview(baseContentView)
    }
    
    @objc
    public func layout() {
        /// 布局
        addConstraint(layoutConstraintMake(timeView, .left, .equal, contentView, .left))
        addConstraint(layoutConstraintMake(timeView, .top, .equal, contentView, .top))
        addConstraint(layoutConstraintMake(timeView, .right, .equal, contentView, .right))
        timeViewHeightConstraint = layoutConstraintMake(timeView, .height, .equal, nil, .notAnAttribute, 35)
        addConstraint(timeViewHeightConstraint)
        
        addConstraint(layoutConstraintMake(timeLabel, .centerX, .equal, timeView, .centerX))
        addConstraint(layoutConstraintMake(timeLabel, .centerY, .equal, timeView, .centerY))
        
        addConstraint(layoutConstraintMake(baseContentView, .left, .equal, contentView, .left))
        addConstraint(layoutConstraintMake(baseContentView, .right, .equal, contentView, .right))
        addConstraint(layoutConstraintMake(baseContentView, .bottom, .equal, contentView, .bottom, -5))
        baseContentViewTopConstraint = layoutConstraintMake(baseContentView, .top, .equal, timeView, .bottom, 5)
        addConstraint(baseContentViewTopConstraint)
    }
}


