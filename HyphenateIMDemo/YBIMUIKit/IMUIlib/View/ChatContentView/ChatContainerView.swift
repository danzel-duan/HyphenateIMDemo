//
/*******************************************************************************
        
        File name:     ChatContainerView.swift
        Author:        danzel_duan
        Gitee.com:     https://gitee.com/danzel-duan/YBIMKit
        E-mail:        danzel_duan@163.com
        
        Description: Copyright © danzel_duan. All rights reserved.
        
        Created by danzel on 2018/12/31.
        
********************************************************************************/
       

import UIKit

public class ChatContainerView: UICollectionView {

    public override init(frame: CGRect, collectionViewLayout layout: UICollectionViewLayout) {
        super.init(frame: frame, collectionViewLayout: layout)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public var currentUpdate: ChatContentViewUpdate? {
        return _currentUpdate
    }
    
    private var _currentUpdate: ChatContentViewUpdate?
    
    func performBatchUpdates(with update: ChatContentViewUpdate, _ isInsert: Bool = false, completion:((Bool) -> Void)?) {
        // read changes
        guard let changes = update.updateChanges else {
            return
        }
        _currentUpdate = update
        // TODO: 不是最优
        if update.updateItems.count > 0 {
            for item in update.updateItems {
                switch item {
                case .update:
                    self.performBatchUpdates({
                        self.reloadItems(at: [IndexPath(row: item.at, section: 0)])
                    }, completion: nil)
                    return
                default:
                    break
                }
            }
        }
        
        var oldContent = self.contentSize
        
        UIView.animate(withDuration: 0) {
            if isInsert {
                self.isHidden = true
            }
            self.performBatchUpdates({
                changes.filter({ $0.isMove }).forEach({
                    self.moveItem(at: .init(item: max($0.from, 0), section: 0),
                                  to: .init(item: max($0.to, 0), section: 0))
                })
                self.insertItems(at: changes.filter({ $0.isInsert }).map({ .init(item: max($0.to, 0), section: 0) }))
                self.reloadItems(at: changes.filter({ $0.isUpdate }).map({ .init(item: max($0.from, 0), section: 0) }))
                self.deleteItems(at: changes.filter({ $0.isRemove }).map({ .init(item: max($0.from, 0), section: 0) }))
                
            }, completion: { finished in
                if isInsert {
                    UIView.animate(withDuration: 0, animations: {
                        if self.contentSize.height > oldContent.height && oldContent.height != 0 {
                            self.setContentOffset(CGPoint(x: 0, y: self.contentSize.height - oldContent.height), animated: false)
                            self.layoutIfNeeded()
                            oldContent = self.contentSize
                        }
                    })
                    self.isHidden = false
                }
                completion?(finished)
            })
        }
        _currentUpdate = nil
    }
}
