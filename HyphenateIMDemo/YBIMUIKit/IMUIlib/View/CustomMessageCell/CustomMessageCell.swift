//
/*******************************************************************************
        
        File name:     CustomMessageCell.swift
        Author:        danzel_duan
        Gitee.com:     https://gitee.com/danzel-duan/YBIMKit
        E-mail:        danzel_duan@163.com
        
        Description: Copyright © danzel_duan. All rights reserved.
        
        Created by danzel on 2018/12/31.
        
********************************************************************************/
       

import UIKit

////- 新增stateView消息状态View(功能设计)
//  ┌───────────────────────────────────────────────────────────┐
//  │    ┌─────────────────────────────────────────────────┐    │
//  │    │          timeLabel:show time                    │    │
//  │    └─────────────────────────────────────────────────┘    │
//  ┣───────────────────────────────────────────────────────────┫
//  ┣───────────────────────────────────────────────────────────┫
//  │ ┌───┐ ######                                              │
//  │ │   │ ┌─────────────────────────────────────┐             │
//  │ └───┘ │                                     │             │
//  │       │                                     │             │
//  │       │         messageContentView          │ ┌─────────┐ │
//  │       │                                     │ │stateView│ │
//  │       │                                     │ └─────────┘ │
//  │       │                                     │             │
//  │       └─────────────────────────────────────┘             │
//  ┣───────────────────────────────────────────────────────────┫
//  └───────────────────────────────────────────────────────────┘

public class CustomMessageCell: MessageCell {
    
    public weak var delegate: MessageDelegate?
    public var stateView: UIView! /// 消息状态View
    
    private var stateViewLeftConstraint: NSLayoutConstraint!
    private var stateViewRightConstraint: NSLayoutConstraint!
    
    @NSManaged fileprivate var _layoutAttributes: ChatContentViewLayoutAttributes?
    
    private var contentCustomView: MessageContentViewable?
    private var contentCustomViewWidthContraint: NSLayoutConstraint!

    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override public var isAdjustItemSiteAtLeft: Bool {
        didSet {
            super.isAdjustItemSiteAtLeft = isAdjustItemSiteAtLeft
            if oldValue == isAdjustItemSiteAtLeft { return }
            if !isAdjustItemSiteAtLeft {
                NSLayoutConstraint.deactivate([stateViewLeftConstraint])
                addConstraint(stateViewRightConstraint)
            } else {
                NSLayoutConstraint.deactivate([stateViewRightConstraint])
                addConstraint(stateViewLeftConstraint)
            }
        }
    }
    
    override public func apply(_ layoutAttributes: UICollectionViewLayoutAttributes) {
        super.apply(layoutAttributes)
        guard let _ = layoutAttributes as? ChatContentViewLayoutAttributes else { return }
        guard let message = self._layoutAttributes?.message else { return }
        
        updateViewLayouts(message: message)
    }
    
    public func updateValue() {
        guard let message = self._layoutAttributes?.message else { return }
        
        message.content.delegate = delegate
        //设置头像的messageDelegate为当前cell的messageDelegate
        avatarView.delegate = delegate
        timeLabel.apply(message)
        avatarView.apply(message)
        contentCustomView?.apply(message)
        nickNameLabel.text = message.name
    }
}

//MARK: 设置UI
extension CustomMessageCell {
    override public func prepare() {
        super.prepare()
        stateView = UIView.init()
        baseContentView.addSubview(stateView)
    }
    
    override public func layout() {
        super.layout()
        stateViewLeftConstraint = layoutConstraintMake(stateView, .left, .equal, messageContentView, .right, 6)
        stateViewRightConstraint = layoutConstraintMake(stateView, .right, .equal, messageContentView, .left, -6)
        addConstraint(stateViewLeftConstraint)
        addConstraint(layoutConstraintMake(stateView, .centerY, .equal, messageContentView, .centerY))
        addConstraint(layoutConstraintMake(stateView, .width, .greaterThanOrEqual, nil, .notAnAttribute, 15))
        addConstraint(layoutConstraintMake(stateView, .height, .equal, nil, .notAnAttribute, 15))
    }
    
    /// 更新视图
    ///
    /// - Parameter message: Messageable
    private func updateViewLayouts(message: Messageable) {
        let view_width = self._layoutAttributes?.info?.layoutedForRect().size.width ?? 0
        
        if contentCustomView == nil {
            contentCustomView = type(of: message.content).viewType.init()
            guard let view = contentCustomView as? UIView, view.superview == nil else { return }
            messageContentView.addSubview(view)
            
            contentCustomViewWidthContraint = layoutConstraintMake(view, .width, .equal, nil, .notAnAttribute, view_width)
            addConstraint(layoutConstraintMake(view, .left, .equal, messageContentView, .left))
            addConstraint(layoutConstraintMake(view, .top, .equal, messageContentView, .top))
            addConstraint(layoutConstraintMake(view, .bottom, .equal, messageContentView, .bottom))
            addConstraint(layoutConstraintMake(view, .right, .equal, messageContentView, .right))
            addConstraint(contentCustomViewWidthContraint)
        } else {
            contentCustomViewWidthContraint.constant = view_width
        }
        
        ///内容对齐方式
        let options = message.options
        if options.alignment == .left {
            isAdjustItemSiteAtLeft = true
        } else if options.alignment == .right {
            isAdjustItemSiteAtLeft = false
        }
        contentCustomView?.apply(message)
    }
}

fileprivate extension UICollectionReusableView {
    @NSManaged fileprivate func _setLayoutAttributes(_ layoutAttributes: UICollectionViewLayoutAttributes)
}
