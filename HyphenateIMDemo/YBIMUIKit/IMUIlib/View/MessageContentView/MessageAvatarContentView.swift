//
/*******************************************************************************
        
        File name:     MessageContentAvatarView.swift
        Author:        danzel_duan
        Gitee.com:     https://gitee.com/danzel-duan/YBIMKit
        E-mail:        danzel_duan@163.com
        
        Description: Copyright © danzel_duan. All rights reserved.
        
        Created by danzel on 2018/12/31.
        
********************************************************************************/
       

import UIKit

/// 头像视图
public class MessageAvatarContentView: UIImageView, MessageContentViewable {
    
    public weak var delegate: MessageDelegate?
    private var message: Messageable!
 
    override init(frame: CGRect) {
        super.init(frame: frame)
        prepare()
    }
    
    required public init () {
        super.init(frame: CGRect.zero)
        prepare()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc
    private func tapGestureRecognizer(sender: UITapGestureRecognizer) {
        delegate?.tapAvatarView(message: message)
    }
    
    @objc
    private func longPressGestureRecognizer(sender: UILongPressGestureRecognizer) {
        if sender.state == .began {
            delegate?.longPressAvatarView(message: message)
        }
    }
    
    public func apply(_ message: Messageable) {
        self.message = message
        image = message.senderAvator
    }
}

//MARK: UI
extension MessageAvatarContentView {
    private func prepare() {
        isUserInteractionEnabled = true
        layer.cornerRadius = 4
        layer.masksToBounds = true
        
        let tap = UITapGestureRecognizer.init(target: self, action: #selector(tapGestureRecognizer(sender:)))
        addGestureRecognizer(tap)
        
        let longPress = UILongPressGestureRecognizer.init(target: self, action: #selector(longPressGestureRecognizer(sender:)))
        longPress.minimumPressDuration = 0.4
        addGestureRecognizer(longPress)
    }
}
