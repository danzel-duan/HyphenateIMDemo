//
/*******************************************************************************
        
        File name:     MessageImageContentView.swift
        Author:        danzel_duan
        Gitee.com:     https://gitee.com/danzel-duan/YBIMKit
        E-mail:        danzel_duan@163.com
        
        Description: Copyright © danzel_duan. All rights reserved.
        
        Created by danzel on 2018/12/31.
        
********************************************************************************/
       

import UIKit

public class MessageImageContentView: UIImageView, MessageContentViewable {
    
    private weak var delegate: MessageDelegate?
    private var message: Messageable?
    
    
    
    public required init() {
        super.init(frame: CGRect.zero)
        prepare()
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        prepare()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc
    private func tapGestureRecognizer(sender: UITapGestureRecognizer) {
        guard let message = self.message else { return }
        delegate?.clickImageView(message: message, image: image) //让代理去实现
    }
    
    public func apply(_ message: Messageable) {
        guard let content = message.content as? MessageImageContent else {
            return
        }
        image = content.image
        self.message = message
        delegate = content.delegate
    }
}

//MARK: UI
extension MessageImageContentView {
    private func prepare() {
        isUserInteractionEnabled = true
        layer.cornerRadius = 5
        layer.masksToBounds = true
        
        let tap = UITapGestureRecognizer.init(target: self, action: #selector(tapGestureRecognizer(sender:)))
        addGestureRecognizer(tap)
    }
}
