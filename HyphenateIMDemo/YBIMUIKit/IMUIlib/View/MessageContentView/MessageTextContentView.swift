//
/*******************************************************************************
        
        File name:     MessageTextContentView.swift
        Author:        danzel_duan
        Gitee.com:     https://gitee.com/danzel-duan/YBIMKit
        E-mail:        danzel_duan@163.com
        
        Description: Copyright © danzel_duan. All rights reserved.
        
        Created by danzel on 2018/12/31.
        
********************************************************************************/
       

import UIKit

public class MessageTextContentView: IMUILabel, MessageContentViewable {

    override init(frame: CGRect) {
        super.init(frame: frame)
        prepare()
    }
    
    public required init() {
        super.init(frame: CGRect.zero)
        prepare()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public func apply(_ message: Messageable) {
        guard let content = message.content as? MessageTextContent else {
            return
        }
        attributedText = content.text
        if message.options.alignment == .left {
            textAlignment = .right
        } else {
            textAlignment = .left
        }
    }
}

//MARK: UI
extension MessageTextContentView {
    private func prepare() {
        textInsets = UIEdgeInsets.init(top: 3, left: 10, bottom: 3, right: 10)
        numberOfLines = 0
        font = UIFont.systemFont(ofSize: 16)
        textColor = UIColor.black
        backgroundColor = .green
        layer.cornerRadius = 10
        layer.masksToBounds = true
    }
}

