//
/*******************************************************************************
 
 File name:     MessageTimeContentView.swift
 Author:        danzel_duan
 Gitee.com:     https://gitee.com/danzel-duan/YBIMKit
 E-mail:        danzel_duan@163.com
 
 Description: Copyright © danzel_duan. All rights reserved.
 
 Created by danzel on 2018/12/31.
 
 ********************************************************************************/

import UIKit

public class MessageTimeContentView: IMUILabel, MessageContentViewable {
    override init(frame: CGRect) {
        super.init(frame: frame)
        prepare()
    }
    
    public required init() {
        super.init(frame: CGRect.zero)
        prepare()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public func apply(_ message: Messageable) {
        let content = MessageTimeContent.init(date: message.time)
        text = content.text
    }
}

//MARK: UI
extension MessageTimeContentView {
    private func prepare() {
        font = UIFont.systemFont(ofSize: 14)
        textColor = UIColor.black
        layer.cornerRadius = 3
        layer.masksToBounds = true
        textAlignment = .center
        backgroundColor = UIColor.hexadecimalColor(hex: 0xD7DCE2)
    }
}
