//
//  ConversationViewCell.swift
//  HyphenateIMDemo
//
//  Created by Shang Hai Rui Xi on 2019/3/8.
//  Copyright © 2019 Shang Hai Rui Xi. All rights reserved.
//

import UIKit

class ConversationViewCell: UITableViewCell {
    
    public var avatarView: UIImageView!
    public var detailLabel: UILabel!
    public var timeLabel: UILabel!
    public var titleLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setUpUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}

extension ConversationViewCell {
    private func setUpUI() {
        avatarView = UIImageView.init()
        avatarView.backgroundColor = UIColor.hexadecimalColor(hex: 0xD7D7D7)
        avatarView.layer.cornerRadius = 3
//        avatarView.layer.masksToBounds = true
        contentView.addSubview(avatarView)
        
        addConstraint(layoutConstraintMake(avatarView, .left, .equal, contentView, .left, 16))
        addConstraint(layoutConstraintMake(avatarView, .centerY, .equal, contentView, .centerY))
        addConstraint(layoutConstraintMake(avatarView, .width, .equal, nil, .notAnAttribute, 48))
        addConstraint(layoutConstraintMake(avatarView, .height, .equal, nil, .notAnAttribute, 48))
        
        titleLabel = UILabel.init()
        titleLabel.font = UIFont.systemFont(ofSize: 20)
        titleLabel.textColor = UIColor.black
        titleLabel.textAlignment = .left
        contentView.addSubview(titleLabel)
        
        addConstraint(layoutConstraintMake(titleLabel, .left, .equal, avatarView, .right, 15))
        addConstraint(layoutConstraintMake(titleLabel, .top, .equal, avatarView, .top, 0))
        
        detailLabel = UILabel.init()
        detailLabel.font = UIFont.systemFont(ofSize: 15)
        detailLabel.textColor = UIColor.hexadecimalColor(hex: 0x767676)
        detailLabel.textAlignment = .left
        contentView.addSubview(detailLabel)
        
        addConstraint(layoutConstraintMake(detailLabel, .left, .equal, avatarView, .right, 15))
        addConstraint(layoutConstraintMake(detailLabel, .bottom, .equal, avatarView, .bottom, 0))
        addConstraint(layoutConstraintMake(detailLabel, .right, .equal, contentView, .right, -15))
        
        timeLabel = UILabel.init()
        timeLabel.font = UIFont.systemFont(ofSize: 14)
        timeLabel.textColor = UIColor.hexadecimalColor(hex: 0x767676)
        timeLabel.textAlignment = .right
        contentView.addSubview(timeLabel)
        
        addConstraint(layoutConstraintMake(timeLabel, .top, .equal, titleLabel, .top, -2))
        addConstraint(layoutConstraintMake(timeLabel, .right, .equal, contentView, .right, -15))
    }
}
