//
/*******************************************************************************
        
        File name:     IMUIlib+OtherChatModule.swift
        Author:        danzel_duan
        Gitee.com:     https://gitee.com/danzel-duan/YBIMKit
        E-mail:        danzel_duan@163.com
        
        Description: Copyright © danzel_duan. All rights reserved.
        
        Created by danzel on 2018/12/31.
        
********************************************************************************/
       
/// - 用于对接任何一个聊天体系

import Foundation

public typealias MSGUser = UserModel
