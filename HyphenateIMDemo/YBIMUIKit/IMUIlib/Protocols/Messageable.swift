//
/*******************************************************************************
        
        File name:     Messageable.swift
        Author:        danzel_duan
        Gitee.com:     https://gitee.com/danzel-duan/YBIMKit
        E-mail:        danzel_duan@163.com
        
        Description: Copyright © danzel_duan. All rights reserved.
        
        Created by danzel on 2018/12/31.
        
********************************************************************************/
       

import UIKit

/// TargetType
///
/// - single: single description
/// - group: group description
@objc
public enum MessageTargetType: Int {
    case single
    case group
    case chatRoom
}

@objc 
public protocol Messageable: class {
    var name: String { get }
    var identifier: UUID { get }
    var msgId: String { get }
    var time: Date { get }
    var sender: MSGUser? { get }
    var senderAvator: UIImage? { get }
    var receiver: MSGUser? { get }
    var content: MessageContentable { get set }
    var options: MessageOptions { get }
    var updateSizeIfNeeded: Bool { get set }
    var unreadCount: Int { get }
    var targetType: MessageTargetType { get }
}

@objc 
public protocol MessageDelegate: NSObjectProtocol {
    func clickImageView(message: Messageable, image: UIImage?)
    func longPressAvatarView(message: Messageable)
    func tapAvatarView(message: Messageable)
}

extension MessageDelegate {
    public func clickImageView(message: Messageable, image: UIImage?) {}
    public func longPressAvatarView(message: Messageable) {}
    public func tapAvatarView(message: Messageable) {}
}
