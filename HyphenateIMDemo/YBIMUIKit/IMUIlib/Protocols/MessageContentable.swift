//
/*******************************************************************************
        
        File name:     MessageContentable.swift
        Author:        danzel_duan
        Gitee.com:     https://gitee.com/danzel-duan/YBIMKit
        E-mail:        danzel_duan@163.com
        
        Description: Copyright © danzel_duan. All rights reserved.
        
        Created by danzel on 2018/12/31.
        
********************************************************************************/
       

import UIKit
@objc
public protocol MessageContentable: class {
    
    static var viewType: MessageContentViewable.Type { get }
    var delegate: MessageDelegate? { get set }
    var layoutMargins: UIEdgeInsets { get }
    
    func sizeThatFits() -> CGSize
}

@objc 
public protocol MessageContentViewable: class { 
    init()
    func apply(_ message: Messageable)
}
