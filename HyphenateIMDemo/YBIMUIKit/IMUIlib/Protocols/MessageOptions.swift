//
/*******************************************************************************
        
        File name:     MessageOptions.swift
        Author:        danzel_duan
        Gitee.com:     https://gitee.com/danzel-duan/YBIMKit
        E-mail:        danzel_duan@163.com
        
        Description: Copyright © danzel_duan. All rights reserved.
        
        Created by danzel on 2018/12/31.
        
********************************************************************************/
       

import UIKit

/// 消息类型
///
/// - chat: 聊天消息
public enum MessageType {
    case notice
    case single
    case group
}

/// 消息对齐方式
///
/// - left: left
/// - center: center
/// - right: right
public enum MessageAlignment {
    case left
    case center
    case right
}

/// 消息状态
///
/// - sending: sending description
/// - sendError: sendError description
/// - sendSucceed: sendSucceed description
/// - downloadFailed: downloadFailed description
public enum MessageState {
    case sending
    case sendError
    case sendSucceed
    case downloadFailed
    case none
}


/// 消息选项
@objc 
public class MessageOptions: NSObject {
    public var type: MessageType = .single
    public var alignment: MessageAlignment = .left
    public var state: MessageState = .none
    public var isShowTime: Bool = false
    
    public convenience init(with content: MessageContentable) {
        self.init()
    }
}
