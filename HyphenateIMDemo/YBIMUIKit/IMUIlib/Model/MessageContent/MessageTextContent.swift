//
/*******************************************************************************
        
        File name:     MessageTextContent.swift
        Author:        danzel_duan
        Gitee.com:     https://gitee.com/danzel-duan/YBIMKit
        E-mail:        danzel_duan@163.com
        
        Description: Copyright © danzel_duan. All rights reserved.
        
        Created by danzel on 2018/12/31.
        
********************************************************************************/
       

import UIKit
// - 内容最大宽度
let contentMaxWidth = UIScreen.main.bounds.size.width * 0.687

public class MessageTextContent: NSObject, MessageContentable {
    
    public var text: NSAttributedString
    
    public weak var delegate: MessageDelegate?
    
    public var layoutMargins: UIEdgeInsets = UIEdgeInsets.init(top: 8, left: 8, bottom: 8, right: 8)
    
    public static var viewType: MessageContentViewable.Type {
        return MessageTextContentView.self
    }
    
    public init(text: String) {
        self.text = NSAttributedString.init(string: text)
    }
    
    public init(text: NSAttributedString) {
        self.text = text
    }
    
    public func sizeThatFits() -> CGSize {
        let mattr = NSMutableAttributedString.init(attributedString: text)
        mattr.addAttribute(.font, value: UIFont.systemFont(ofSize: 16), range: NSMakeRange(0, mattr.length))
        //TODO: 宽度要处理一下
        let mattrSize = mattr.boundingRect(with: CGSize(width: contentMaxWidth, height: CGFloat(MAXFLOAT)), options: [.usesLineFragmentOrigin,.usesFontLeading], context: nil)
        return .init(width: max(mattrSize.width + 25, 20), height: max(mattrSize.height - 15, 20))
    }
}
