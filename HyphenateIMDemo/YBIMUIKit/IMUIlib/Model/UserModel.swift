//
//  UserModel.swift
//  HyphenateIMDemo
//
//  Created by danzel on 2019/1/3.
//  Copyright © 2019 Shang Hai Rui Xi. All rights reserved.
//

import Foundation

public class UserModel: NSObject {
    public var userid: String  //用户id
    public var nickname: String //用户昵称
    public var avatarURL: String //用户头像url
    public var avatarImage: UIImage //用户头像
    
    public init(userid: String?) {
        self.userid = userid ?? ""
        nickname = "duanzhenglong"
        avatarURL = ""
        avatarImage = UIImage.init(named: "index.jpg")!
        super.init()
    }
}
