//
//  ExMessage.swift
//  HyphenateIMDemo
//
//  Created by danzel on 2019/1/3.
//  Copyright © 2019 Shang Hai Rui Xi. All rights reserved.
//

import Foundation

public final class ExMessage<Base> {
    public let base: Base
    public init(_ base: Base) {
        self.base = base
    }
}

/**
 A type that has ExJMessage extensions.
 */
public protocol ExMessageCompatible {}

public extension ExMessageCompatible {
    public static var ex: ExMessage<Self>.Type {
        get { return ExMessage.self }
    }
    public var ex: ExMessage<Self> {
        get { return ExMessage(self) }
    }
}
