//
/*******************************************************************************
        
        File name:     MessageData.swift
        Author:        danzel_duan
        Gitee.com:     https://gitee.com/danzel-duan/YBIMKit
        E-mail:        danzel_duan@163.com
        
        Description: Copyright © danzel_duan. All rights reserved.
        
        Created by danzel on 2018/12/31.
        
********************************************************************************/
       

import Foundation

/// 用于存放消息的数组数据
public class MessageData: NSCopying {
    
    internal var elements: [Messageable]
    
    init() {
        self.elements = []
    }
    
    internal init(_ elements: [Messageable]) {
        self.elements = elements
    }
    
    internal var count: Int {
        return elements.count
    }
    
    public func copy(with zone: NSZone? = nil) -> Any {
        return MessageData.init(self.elements)
    }
    
    internal subscript(index: Int) -> Messageable {
        return elements[index]
    }
    
    
    internal func subarray(with subrange: Range<Int>) -> Array<Messageable> {
        return Array(elements[subrange])
    }
    
    internal func replaceSubrange(_ subrange: Range<Int>, with collection: Array<Messageable>)  {
        elements.replaceSubrange(subrange, with: collection)
    }
}
