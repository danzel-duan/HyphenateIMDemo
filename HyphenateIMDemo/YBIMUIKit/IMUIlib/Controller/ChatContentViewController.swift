//
//  ChatContentViewController.swift
//  YBIMKit
//
//  Created by Shang Hai Rui Xi on 2019/1/3.
//  Copyright © 2019 Shanghai Three-Cobbler Technology Service Co.Ltd. All rights reserved.
//

import UIKit

/// 聊天会话内容界面
public class ChatContentViewController: UIViewController, MYKeyboardInputViewDelegate {
    
    func keyboardInputViewFrameDidChange(variation height: CGFloat) {
        chatContentView.contentInset.bottom = height
        chatContentView.scrollToLastMessage()
    }

    func keyboardOutPutAttribute(_ attribute: NSAttributedString) {}
    func functionView(view: FunctionsView, didSelectFunctionType type: FunctionType) {}
    
    public var chatContentView: ChatContentView!
    public var messages: [Messageable] = []
    public var inputTextField: MYKeyboardInputView!
    
    override public func loadView() {
        super.loadView()
        chatContentView = ChatContentView.init(frame: CGRect.init(x: 0, y: 0, width: view.frame.size.width, height: view.frame.size.height), layout: ChatContentViewLayout.init())
        chatContentView.messageDelegate = self
        chatContentView.delegate = self
        view = chatContentView
    }

    override public func viewDidLoad() {
        super.viewDidLoad()
        inputTextField = MYKeyboardInputView.init()
        let height = inputTextField.defineHeight
        inputTextField.frame = CGRect.init(x: 0, y: MYSCREEN_HEIGHT - height, width: UIScreen.main.bounds.width, height: height)
        inputTextField.delegate = self
        inputTextField.initFrame = inputTextField.frame
        view.addSubview(inputTextField)

        chatContentView.contentInset.bottom = height
    }
    
    ///TODO: 这些代理方法在子类重载实现时不能放在子类的扩展里
    /// 点击用户头像
    ///
    /// - Parameter message: message
    public func tapAvatarView(message: Messageable) {}
    /// 长按用户头像
    ///
    /// - Parameter message: message
    public func longPressAvatarView(message: Messageable) {}
    /// 点击内容中图片
    ///
    /// - Parameters:
    ///   - message: message
    ///   - image: image
    public func clickImageView(message: Messageable, image: UIImage?) {}
    /// 刷新新数据（配合使用chatView.stopRefresh进行停止刷新）
    ///
    /// - Parameter chatView: chatView
    public func refreshChatContentView(chatView: ChatContentView) {}
    
    deinit {
        print("ChatContentViewController释放了")
    }
}

extension ChatContentViewController: MessageDelegate, ChatContentViewDelegate {}
