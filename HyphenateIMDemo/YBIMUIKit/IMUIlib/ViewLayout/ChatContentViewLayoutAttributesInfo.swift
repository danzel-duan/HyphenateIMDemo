//
/*******************************************************************************
        
        File name:     ChatContentViewLayoutAttributesInfo.swift
        Author:        danzel_duan
        Gitee.com:     https://gitee.com/danzel-duan/YBIMKit
        E-mail:        danzel_duan@163.com
        
        Description: Copyright © danzel_duan. All rights reserved.
        
        Created by danzel on 2019/1/1.
        
********************************************************************************/
       

import UIKit

public class ChatContentViewLayoutAttributesInfo: NSObject {

    public var message: Messageable {
        return _message
    }
    private var _message: Messageable
    private var size: CGSize
    private var rect: CGRect
    
    init(message: Messageable, size: CGSize, rect: CGRect) {
        self._message = message
        self.size = size
        self.rect = rect
        super.init()
    }
    
    public func layoutedForRect() -> CGRect {
        return rect
    }
    
    public func layoutedForSize() -> CGSize {
        return size
    }
}
