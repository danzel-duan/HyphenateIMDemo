//
/*******************************************************************************
        
        File name:     ChatContentViewLayoutAttributes.swift
        Author:        danzel_duan
        Gitee.com:     https://gitee.com/danzel-duan/YBIMKit
        E-mail:        danzel_duan@163.com
        
        Description: Copyright © danzel_duan. All rights reserved.
        
        Created by danzel on 2019/1/1.
        
********************************************************************************/
       

import UIKit

public class ChatContentViewLayoutAttributes: UICollectionViewLayoutAttributes {

    public var info: ChatContentViewLayoutAttributesInfo?
    public var message: Messageable? {
        return info?.message
    }
    
    public override func copy(with zone: NSZone? = nil) -> Any {
        let new = super.copy(with: zone)
        if let new = new as? ChatContentViewLayoutAttributes {
            new.info = info
        }
        return new
    }
}
