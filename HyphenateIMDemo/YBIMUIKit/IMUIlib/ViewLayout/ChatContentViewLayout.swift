//
/*******************************************************************************
        
        File name:     ChatContentViewLayout.swift
        Author:        danzel_duan
        Gitee.com:     https://gitee.com/danzel-duan/YBIMKit
        E-mail:        danzel_duan@163.com
        
        Description: Copyright © danzel_duan. All rights reserved.
        
        Created by danzel on 2018/12/31.
        
********************************************************************************/
       

import UIKit

/// 布局协议
public protocol ChatContentViewDelegateLayout: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, viewLayout collectionViewLayout: ChatContentViewLayout, itemAt indexPath: IndexPath) -> Messageable
}

/// 布局
public class ChatContentViewLayout: UICollectionViewFlowLayout {
    
    private var allLayoutAttributesInfo: [UUID: ChatContentViewLayoutAttributesInfo] = [:]

    public override init() {
        super.init()
        
        minimumLineSpacing = 5
        minimumInteritemSpacing = 0
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        minimumLineSpacing = 5
        minimumInteritemSpacing = 0
        fatalError("init(coder:) has not been implemented")
    }
    
    public override class var layoutAttributesClass: AnyClass {
        return ChatContentViewLayoutAttributes.self
    }
    
    public override func layoutAttributesForItem(at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        let layoutAttributes = super.layoutAttributesForItem(at: indexPath)
        if let layoutAttributes = layoutAttributes as? ChatContentViewLayoutAttributes, layoutAttributes.info == nil {
            layoutAttributes.info = layoutAttributesInfoForItem(at: indexPath)
        }
        return layoutAttributes
    }
    
    public override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        let attributesArr = super.layoutAttributesForElements(in: rect)
        attributesArr?.forEach({
            if let layoutAttributes = $0 as? ChatContentViewLayoutAttributes, layoutAttributes.info == nil {
                layoutAttributes.info = layoutAttributesInfoForItem(at: layoutAttributes.indexPath)
            }
        })
        return attributesArr
    }
}

extension ChatContentViewLayout {
    
    public func layoutAttributesInfoForItem(at indexPath: IndexPath) -> ChatContentViewLayoutAttributesInfo? {
        guard let message = getMessage(at: indexPath) else {
            return nil
        }
        if let info = allLayoutAttributesInfo[message.identifier] {
            if !message.updateSizeIfNeeded {
                return info
            }
        }
        
        var contetSize = message.content.sizeThatFits()
        
        let rect = CGRect.init(origin: CGPoint.init(), size: contetSize)

        let spece: CGFloat = 45 + 35 // + 10/25(隐藏名字/不隐藏名字)
        contetSize.height += spece
        contetSize.height += (message.content.layoutMargins.bottom + message.content.layoutMargins.top)
        
        let info = ChatContentViewLayoutAttributesInfo.init(message: message, size: contetSize, rect: rect)
        allLayoutAttributesInfo[message.identifier] = info
        return info
    }
}

extension ChatContentViewLayout {
    /// 通过代理方法获取message实例
    ///
    /// - Parameter indexPath: indexPath
    /// - Returns: Messageable
    private func getMessage(at indexPath: IndexPath) -> Messageable? {
        guard let collectionView = collectionView, let delegate = collectionView.delegate as? ChatContentViewDelegateLayout else {
            return nil
        }
        return delegate.collectionView(collectionView, viewLayout: self, itemAt: indexPath)
    }
}

/// 协议扩展
extension ChatContentViewDelegateLayout {
    
}
