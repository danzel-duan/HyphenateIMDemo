//
//  FriendViewController.swift
//  HyphenateIMDemo
//
//  Created by Shang Hai Rui Xi on 2019/1/4.
//  Copyright © 2019 Shang Hai Rui Xi. All rights reserved.
//

import UIKit

class FriendViewController: EaseUsersListViewController, EMUserListViewControllerDelegate {
    func userListViewController(_ userListViewController: EaseUsersListViewController!, didSelect userModel: IUserModel!) {
        if (userModel != nil) {
            guard let buddy = userModel.buddy else { return }
            let chatVc = ChatViewController.init(aConversationId: buddy, aType: EMConversationTypeChat)
            chatVc.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(chatVc, animated: true)
        }
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()

        delegate = self
    }

}
