//
//  ChatViewController.swift
//  HyphenateIMDemo
//
//  Created by Shang Hai Rui Xi on 2019/1/3.
//  Copyright © 2019 Shang Hai Rui Xi. All rights reserved.
//

import UIKit

class ChatViewController1: EaseMessageViewController, EMClientDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.showRefreshHeader = true;
        self.delegate = self;
        self.dataSource = self;
        if conversation.type == EMConversationTypeChat {
            let uDefaults = UserDefaults.standard
            let isTyping = uDefaults.bool(forKey: "MessageShowTyping")
            if isTyping {
                NotificationCenter.default.addObserver(self, selector: #selector(keyBoardWillHide(noti:)), name: UIResponder.keyboardWillHideNotification, object: nil)
            }
        }
    }
    
    deinit {
        EMClient.shared()?.removeDelegate(self)
    }
    
    override func tableViewDidTriggerHeaderRefresh() {
        EMClient.shared()?.chatManager.asyncFetchHistoryMessages(fromServer: self.conversation.conversationId, conversationType: self.conversation.type, startMessageId: "0", pageSize: 10, completion: { (_, _) in
            super.tableViewDidTriggerHeaderRefresh()
        })
    }
    
}

extension ChatViewController1: EaseMessageViewControllerDelegate, EaseMessageViewControllerDataSource {

    func messageViewController(_ viewController: EaseMessageViewController!, didSelectAvatarMessageModel messageModel: IMessageModel!) {
        
    }
}


extension ChatViewController1 {
    @objc private func keyBoardWillHide(noti: Notification) {
        
    }
}
