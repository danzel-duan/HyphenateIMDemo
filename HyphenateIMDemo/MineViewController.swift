//
//  MineViewController.swift
//  HyphenateIMDemo
//
//  Created by Shang Hai Rui Xi on 2019/1/4.
//  Copyright © 2019 Shang Hai Rui Xi. All rights reserved.
//

import UIKit

class MineViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    
    var headerView: MineHeaderView!
    
    var icons: [[String]] = [["qb"], ["hy", "gl", "rw"], ["kf", "sz"]]
    var titles: [[String]] = [["钱包"], ["会员", "攻略", "任务"], ["客服", "设置"]]
    override func viewDidLoad() {
        super.viewDidLoad()
        
        headerView = MineHeaderView.loadFromNib()
        tableView.tableHeaderView = headerView
        tableView.tableFooterView = UIView()
        
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cellID")
        
        let rightItem = UIBarButtonItem.init(title: "好友", style: .plain, target: self, action: #selector(friend))
        navigationItem.rightBarButtonItem = rightItem
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        headerView.frame.size.height = 150
    }
    
    @objc
    func friend() {
        let friendVc = FriendViewController()
        friendVc.hidesBottomBarWhenPushed = true
        navigationController?.pushViewController(friendVc, animated: true)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 1
        case 1:
            return 3
        default:
            return 2
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellID", for: indexPath)

        cell.textLabel?.text = titles[indexPath.section][indexPath.row]
        cell.imageView?.image = UIImage.init(named: icons[indexPath.section][indexPath.row])
        
        cell.accessoryType = .disclosureIndicator
        cell.textLabel?.font = UIFont.systemFont(ofSize: 13)
        cell.selectionStyle = .none
        
        let itemSize = CGSize.init(width: 18, height: 17)
        UIGraphicsBeginImageContext(itemSize)
        let imageRect = CGRect.init(x: 0, y: 0, width: itemSize.width, height: itemSize.height)
        cell.imageView?.image?.draw(in: imageRect)
        cell.imageView?.image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return cell
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 10
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let settingVc = MineSettingViewController()
        settingVc.title = titles[indexPath.section][indexPath.row]
        settingVc.hidesBottomBarWhenPushed = true
        navigationController?.pushViewController(settingVc, animated: true)
    }
}

// 协议
protocol NibLoadable {}
extension UIView: NibLoadable {}

// 加载nib
extension NibLoadable where Self : UIView {
    static func loadFromNib(_ nibname : String? = nil) -> Self {
        let loadName = nibname == nil ? "\(self)" : nibname!
        return Bundle.main.loadNibNamed(loadName, owner: nil, options: nil)?.first as! Self
    }
}
