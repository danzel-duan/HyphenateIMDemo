//
//  ViewController.swift
//  HyphenateIMDemo
//
//  Created by Shang Hai Rui Xi on 2018/12/28.
//  Copyright © 2018 Shang Hai Rui Xi. All rights reserved.
//

import UIKit

class ViewController: UIViewController{
    
    @IBOutlet weak var textField1: UITextField!
    
    @IBOutlet weak var textField2: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
    }
    
    @IBAction func loginClick(_ sender: Any) {
        
        EMClient.shared()?.login(withUsername: textField1.text, password: textField2.text, completion: { (_, error) in
            if error != nil {
                
            } else {
                print("登录成功")
            }
            
            print(error)
        })
        
        textField2.resignFirstResponder()
        textField1.resignFirstResponder()
    }
    
}

