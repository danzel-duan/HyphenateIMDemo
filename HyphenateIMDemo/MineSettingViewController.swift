//
//  MineSettingViewController.swift
//  HyphenateIMDemo
//
//  Created by Shang Hai Rui Xi on 2019/1/4.
//  Copyright © 2019 Shang Hai Rui Xi. All rights reserved.
//

import UIKit

class MineSettingViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = UIColor.white
    }

}
