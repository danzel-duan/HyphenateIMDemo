//
//  File.swift
//  HyphenateIMDemo
//
//  Created by danzel on 2019/1/3.
//  Copyright © 2019 Shang Hai Rui Xi. All rights reserved.
//

import Foundation

extension EMMessage: ExMessageCompatible {}
extension EMConversation: ExMessageCompatible {}
extension EMOptions: ExMessageCompatible {}

extension EMMessage {
    
    typealias Callback = (EMMessage, Data) -> Void
    
    func parseMessage(updateMediaMessage: Callback? = nil) -> Message {
        
        var msg: Message
        let currentUser = UserModel.getMyInfo()
        let fromUser = UserModel.getUserInfo(userid: from)
        let state = ex.state
        let isGroup = chatType == EMChatTypeGroupChat
        
        let contentType = body.type
        
        switch contentType {
        case EMMessageBodyTypeText:
            let content = body as! EMTextMessageBody
            msg = Message.init(content: MessageTextContent.init(text: content.text))
        case EMMessageBodyTypeImage:
            let content = body as! EMImageMessageBody
            let imageContent = MessageImageContent.init()
            imageContent.imageSize = content.size
            //1.缩略图已下载
            if let thumbnailLocalPath = content.thumbnailLocalPath, let image = UIImage(contentsOfFile: thumbnailLocalPath) {
                imageContent.imageSize = image.size
                imageContent.image = image
            } else {  //2.未下载
                EMClient.shared()?.chatManager.downloadMessageThumbnail(self, progress: nil, completion: { (message, _) in
                    guard let imageBody = message?.body as? EMImageMessageBody else { return }
                    let image = UIImage(contentsOfFile: imageBody.thumbnailLocalPath)
                    let imageData = image?.jpegData(compressionQuality: 1)
                    if let imageData = imageData {
                       updateMediaMessage?(self, imageData)
                    }
                })
            }
            msg = Message.init(content: imageContent)
        default:
            msg = Message.init(content: MessageTextContent.init(text: "default"))
        }
        
        if currentUser.userid == fromUser.userid {  //是自己
            msg.options.alignment = .right
        } else {
            msg.options.alignment = .left
        }
        
        if isGroup {
            msg.targetType = .group
        } else if chatType == EMChatTypeChat {
            msg.targetType = .single
        }
        msg.msgId = messageId
        msg.sender = fromUser
        msg.name = fromUser.nickname
        msg.senderAvator = fromUser.avatarImage
        msg.options.state = state
        msg.time = Date.init(timeIntervalSince1970: TimeInterval(localTime / 1000))
        
        return msg
    }
}

extension ExMessage where Base: EMMessage {
    
    var state: MessageState {
        switch base.status {
        case EMMessageStatusSucceed:
            return .sendSucceed
        case EMMessageStatusDelivering:
            return .sending
        case EMMessageStatusFailed:
            return .sendError
        default:
            return .none
        }
    }
}

extension UserModel {
    //MARK: 获取自己的用户信息
    public static func getMyInfo() -> UserModel {
        let userM = UserModel.init(userid: EMClient.shared()?.currentUsername)
        return userM
    }
    
    //MARK: 获取自己服务器上的用户信息
    public static func getUserInfo(userid: String) -> UserModel {
        let userM = UserModel.init(userid: userid)
        return userM
    }
}
