//
//  Array+Extensions.swift
//  HyphenateIMDemo
//
//  Created by Shang Hai Rui Xi on 2019/2/19.
//  Copyright © 2019 Shang Hai Rui Xi. All rights reserved.
//

import Foundation

// MARK: -
extension Array where Element: Messageable {
    
    func indexs(at message: EMMessage) -> Int? {
        return self.index(where: { (m) -> Bool in
            m.msgId == message.messageId
        })
    }
    
    func index(at message: Messageable) -> Int? {
        return self.index(where: { (m) -> Bool in
            m.msgId == message.msgId
        })
    }
}
