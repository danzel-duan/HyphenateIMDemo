//
/*******************************************************************************
        
        File name:     Message.swift
        Author:        danzel_duan
        Gitee.com:     https://gitee.com/danzel-duan/YBIMKit
        E-mail:        danzel_duan@163.com
        
        Description: Copyright © danzel_duan. All rights reserved.
        
        Created by danzel on 2018/12/31.
        
********************************************************************************/
       

import UIKit

public class Message: Messageable {
    
    init(content: MessageContentable) {
        self.content = content
        self.options = MessageOptions(with: content)
    }
    
    public var sender: MSGUser?
    
    public var senderAvator: UIImage?
    
    public var receiver: MSGUser?
    
    public var content: MessageContentable
    
    public var options: MessageOptions
    
    public var updateSizeIfNeeded: Bool = false
    
    public var unreadCount: Int = 0
    
    public var targetType: MessageTargetType = .single
    
    public var name: String = ""
    
    public var identifier: UUID = .init()
    
    public var msgId: String = ""
    
    public var time: Date = .init()
}


